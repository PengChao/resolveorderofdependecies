class Data:
    pass


class Node:
    def __init__(self, name: str = '', dependencies=[], data=Data()):
        # : list[Node],
        """ name is unique"""
        self.name = name
        self._dependencies = dependencies
        self.data: Data = data
    
    def getDependeies(self):
        #  -> list[Node]
        return self._dependencies
    
    def __repr__(self):
        return f"<{self.name}>"


def resolveOrderOfDependecies(node: Node): 
    # ordered
    # -> Optional[list[node], None]
    pending = set()
    learnt = set()
    res = []

    def dfs(node):
        if node in learnt:
            return
        if node in pending:
            raise ValueError("loop")

        pending.add(node)
        deps = node.getDependeies()
        for d in deps:
            dfs(d)
        learnt.add(node)
        res.append(node)
        pending.discard(node)

    try:
        dfs(node)
    except ValueError:
        return None

    return list(reversed(res))


def main():
    A = Node()
    B = Node()
    C = Node()
    D = Node()
    E = Node()
    F = Node()
    G = Node()

    A.name = "A"
    B.name = "B"
    C.name = "C"
    D.name = "D"
    E.name = "E"
    F.name = "F"
    G.name = "G"

    A._dependencies = [B,C,D,E]
    B._dependencies = [C,D,G,F]
    C._dependencies = [E, F]
    D._dependencies = [C, E, F]
    E._dependencies = [G]
    F._dependencies = []
    G._dependencies = [F]
    
    print(resolveOrderOfDependecies(A))

if __name__ == "__main__":
    main()


# 
# Your previous Plain Text content is preserved below:
# 
# /**
# 
#  A : [ B , C  , D,  E]
#  B : [ C , D , G , F]
#  D : [ C , E, F]
#  C : [ E , F ]
#  G : F
#  F : []
#  E : G
#  
#  [A B C D E F G ]
#      .. D C ...
#  
#  Input : Node { Name, Data, getDependeies() } 
#  Return: vector<Node>

# */
#  
#
